# README #

Our team has come up with a technical test to assess your iOS coding capability. Do follow the requirements and UI /UX design guide line to build the test project. 
Feel free to use your creativity and also add in suggestions to improve the test project.  
Attached below is the UI / UX flow for the test project, you are allowed to make changes to better improve the flow. 

### Main Purpose: 
To identify coder�s coding architecture and practices, as well as database management. Demonstration of concurrency coding is required.

### What is this repository for? ###

* Test Project Title: Local events management application
* Language use: Swift 3.0
* Dependency manger: Cocoa Pods : Realm Database (Realm database will be used as local database, Don't use it with Realm Object Server)

### Requirements: ###

* User must be able to login, logout and sign up
* User must be able to toggle between events and profile main page
* User must be able to toggle between events graph and events list in the events page.
* Event graph must be scrollable horizontally and it sholuld be manipulated to date vs cost.
* User must able to select a cost node in the graph and a summary should be update at the bottom of the graph.
* User must be able to edit events, filter event (pop up screen) based on (Name, Date and cost), create event and delete event
* User must be able to edit profile, add profile picture, add address, edit address, delete address and update address
* User profile picture must be taken from camera and the file size of the image should be downgrated to with a minimal quality loss. (ex: Original image size ~5MB to less than 1MB - 2MB)
* User Interface should be simple. Design do not need to be fancy and assets are not required.
* Every UI components must be created by programatically (Don't use storyboard & xib).
* Strictly avoid using any 3rd party libraries except Ream Database for this application.

### Fork this repo make it as Private and create a new branch from master with your name and do the works on that branch. ###
### When you are done ###

* Send the pull request to create a new branch to this repository, once you are confident with your work. Do take note the maximal period to write the code is 3 days. 
* Sending your work early is a bonus that will greatly helped your assessment.
* Once we received your code, we will get back to you once assessment is done. 

### Feel free to ask questions if you have any doubt. ###

### UI ###

Login Page: 
![alt text][login]

[login]: https://bytebucket.org/omh_ios_test/lema/raw/20ea3d9befb8564ac5658c56244cbc9d2809b5d5/Screenshots/Login.png "Login Page"

Signup Page: 
![alt text][signup]

[signup]: https://bytebucket.org/omh_ios_test/lema/raw/20ea3d9befb8564ac5658c56244cbc9d2809b5d5/Screenshots/Sigup.png "Signup Page"

Event Graph Page: 
![alt text][eventgraph]

[eventgraph]: https://bytebucket.org/omh_ios_test/lema/raw/20ea3d9befb8564ac5658c56244cbc9d2809b5d5/Screenshots/Event%20Graph.png "Event Graph Page"

Event List Page: 
![alt text][eventlist]

[eventlist]: https://bytebucket.org/omh_ios_test/lema/raw/20ea3d9befb8564ac5658c56244cbc9d2809b5d5/Screenshots/Event%20List.png "Event List Page"

Create Event Page: 
![alt text][create_event]

[create_event]: https://bytebucket.org/omh_ios_test/lema/raw/20ea3d9befb8564ac5658c56244cbc9d2809b5d5/Screenshots/Create%20Event.png "Create Event Page"

Edit Event Page: 
![alt text][edit_event]

[edit_event]: https://bytebucket.org/omh_ios_test/lema/raw/20ea3d9befb8564ac5658c56244cbc9d2809b5d5/Screenshots/Edit%20Event.png "Edit Event Page"

User Profile Page: 
![alt text][user_profile]

[user_profile]: https://bytebucket.org/omh_ios_test/lema/raw/20ea3d9befb8564ac5658c56244cbc9d2809b5d5/Screenshots/Profile.png "Profile Page"

Edit Profile Page: 
![alt text][edit_profile]

[edit_profile]: https://bytebucket.org/omh_ios_test/lema/raw/20ea3d9befb8564ac5658c56244cbc9d2809b5d5/Screenshots/Edit%20Profile.png "Edit Profile Page"
